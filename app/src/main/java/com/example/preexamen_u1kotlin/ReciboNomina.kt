package com.example.preexamen_u1kotlin

class ReciboNomina {

    // ATRIBUTOS
    var numRecibo : Int = 0
    var nombre : String = ""
    var horasTrabNormal : Float = 0.0f
    var horasTrabExtras : Float = 0.0f
    var puesto : Int = 0
    var impuestoPorc : Float = 0.16f



    // CONSTRUCTORES
    constructor(){
        this.numRecibo = 0
        this.nombre = ""
        this.horasTrabNormal = 0.0f
        this.horasTrabExtras = 0.0f
        this.puesto = 0
        this.impuestoPorc = 0.16f
    }
    constructor(
        numRecibo:Int,
        nombre:String,
        horasTrabNormal:Float,
        horasTrabExtras:Float,
        puesto:Int,
        impuestoPorc:Float){

        this.numRecibo = numRecibo
        this.nombre = nombre
        this.horasTrabNormal = horasTrabNormal
        this.horasTrabExtras = horasTrabExtras
        this.puesto = puesto
        this.impuestoPorc = impuestoPorc
    }



    // METODOS
    fun calcularSubtotal() : Float{

        //Caso: Puesto seleccionado
        if(this.puesto != 0){

            var pagoBase = 200.0f
            when (puesto){
                1 -> pagoBase += pagoBase*0.2f
                2 -> pagoBase += pagoBase*0.5f
                3 -> pagoBase += pagoBase
            }

            var subTotal = 0.0f
            subTotal = (pagoBase*horasTrabNormal) + (horasTrabExtras*pagoBase*2)

            return  subTotal
        }
        // Caso: Puesto no seleccionado
        else return 0.0f
    }

    fun calcularImpuesto() : Float{
        return this.calcularSubtotal() * this.impuestoPorc
    }

    fun calcularTotal() : Float{
        return this.calcularSubtotal() - this.calcularImpuesto()
    }
}