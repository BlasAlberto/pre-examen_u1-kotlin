package com.example.preexamen_u1kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.util.Random

class Recibo_Activity : AppCompatActivity() {

    // Declaración componentes
    private lateinit var lblUsuario : TextView
    private lateinit var txtNumRecibo : EditText
    private lateinit var txtNombre : EditText
    private lateinit var txtHorasNormal : EditText
    private lateinit var txtHorasExtra : EditText
    private lateinit var rdgPuesto : RadioGroup
    private lateinit var rdbPuesto : RadioButton
    private lateinit var txtSubtotal : EditText
    private lateinit var txtImpuesto : EditText
    private lateinit var txtTotal : EditText
    private lateinit var btnCalcular : Button
    private lateinit var btnLimpiar : Button
    private lateinit var btnRegresar : Button
    // Declaración de clases
    private lateinit var reciboNomina : ReciboNomina


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo)

        // Instaciamiento de componentes
        this.instanciarComponentes()

        // Asignación de metodo clic
        this.btnCalcular.setOnClickListener { btnCalcular_clic() }
        this.btnRegresar.setOnClickListener { btnRegresar_clic() }
        this.btnLimpiar.setOnClickListener { btnLimpiar_clic() }

        // Obtener Usuario con el paquete de información enviado
        var paqueteInf:Bundle = intent.extras as Bundle
        var usuario:String = paqueteInf!!.getString("NombreTrabajador").toString()
        this.lblUsuario.setText(usuario)

        // Obtener nombre desde los Strings guardados
        var nombre: String
        nombre = applicationContext.resources.getString(R.string.NombreTrabajador)
        this.txtNombre.setText(nombre)

        // Generar numero de recibo
        this.numAleatorio()
    }


    // Relación de componentes con el layout
    private fun instanciarComponentes(){
        this.lblUsuario = findViewById(R.id.lblUsuario) as TextView
        this.txtNumRecibo = findViewById(R.id.txtNumeroRecibo) as EditText
        this.txtNombre = findViewById(R.id.txtNombre) as EditText
        this.txtHorasNormal = findViewById(R.id.txtHorasTrabajadas) as EditText
        this.txtHorasExtra = findViewById(R.id.txtHorasExtras) as EditText
        this.rdgPuesto = findViewById(R.id.rgPuestos) as RadioGroup
        this.rdbPuesto = findViewById(rdgPuesto.checkedRadioButtonId)
        this.txtSubtotal = findViewById(R.id.txtSubtotal) as EditText
        this.txtImpuesto = findViewById(R.id.txtImpuesto) as EditText
        this.txtTotal = findViewById(R.id.txtTotalPagar) as EditText
        this.btnCalcular = findViewById(R.id.btnCalcular) as Button
        this.btnLimpiar = findViewById(R.id.btnLimpiar) as Button
        this.btnRegresar = findViewById(R.id.btnRegresar) as Button
    }


    // Metodo clic del Button: btnCalcular
    private fun btnCalcular_clic(){

        var strHorasNormal:String = this.txtHorasNormal.text.toString()
        var strHorasExtra:String = this.txtHorasExtra.text.toString()
        this.reciboNomina = ReciboNomina()

        if(!strHorasNormal.equals("") && !strHorasExtra.equals("")){

           // Obtener horas normales y extras
           this.reciboNomina.horasTrabNormal = strHorasNormal.toFloat()
           this.reciboNomina.horasTrabExtras = strHorasExtra.toFloat()

           // Obtener puesto
           this.rdbPuesto = findViewById(rdgPuesto.checkedRadioButtonId)
           when (rdbPuesto.text.toString()){
               "Auxiliar" -> this.reciboNomina.puesto = 1
               "Albañil" -> this.reciboNomina.puesto = 2
               "Ing. Obra" -> this.reciboNomina.puesto = 3
           }

           // Realizar calculos
           this.txtSubtotal.setText(this.reciboNomina.calcularSubtotal().toString())
           this.txtImpuesto.setText(this.reciboNomina.calcularImpuesto().toString())
           this.txtTotal.setText(this.reciboNomina.calcularTotal().toString())
        }
        else Toast.makeText(applicationContext, "Asegurece de ingresar ambas horas.", Toast.LENGTH_SHORT).show()
    }


    // Metodo clic para el Button: btnLimpiar
    private fun btnLimpiar_clic(){
        this.txtHorasNormal.setText("")
        this.txtHorasExtra.setText("")

        var rdbAuxiliar:RadioButton = findViewById<RadioButton>(R.id.rbAuxiliar)
        rdbAuxiliar.isChecked = true

        this.txtSubtotal.setText("")
        this.txtImpuesto.setText("")
        this.txtTotal.setText("")

        this.reciboNomina = ReciboNomina()

        this.numAleatorio()
    }


    // Metodo clic para el Button: btnRegresar
    private fun btnRegresar_clic(){
        var confirmar = AlertDialog.Builder(this)

        confirmar.setTitle("Pago nomina.")
        confirmar.setMessage("¿Decea regresar al inicio?")

        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }

        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }

        confirmar.show()
    }

    // Metodo para generar un numero aleatorio en el EditText: txtNumeroRecibo
    // Minimo : 10000
    // Maximo : 89999
    private fun numAleatorio(){
        val aleatorio = Random()
        val numRecibo = aleatorio.nextInt(89999)+10000
        this.txtNumRecibo.setText(numRecibo.toString())
    }
}