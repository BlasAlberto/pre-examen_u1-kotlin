package com.example.preexamen_u1kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    // Declaración de componentes
    private lateinit var txtNombreUsuario: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Instnciar componentes
        this.instanciarComponentes()

        // Asignación de eventos clic
        this.btnIngresar.setOnClickListener { btnIngresar_clic() }
        this.btnSalir.setOnClickListener { btnSalir_clic() }
    }


    // Relación de componentes con el layout
    private fun instanciarComponentes(){
        this.txtNombreUsuario = findViewById(R.id.txtNombreTrabajador)
        this.btnIngresar = findViewById(R.id.btnIngresar)
        this.btnSalir = findViewById(R.id.btnSalir)
    }

    // Metodo clic para el Button: btnIngresar
    private fun btnIngresar_clic(){
        if(!this.txtNombreUsuario.text.toString().equals("")){

            // Creación del paquete de información
            var bundle = Bundle()
            bundle.putString("NombreTrabajador", this.txtNombreUsuario.text.toString())

            // Creación de Intent para llamar a otra actividad
            var intento = Intent(this@MainActivity, Recibo_Activity::class.java)
            intento.putExtras(bundle)

            // Iniciar la actividad
            startActivity(intento)

            // Reiniciar MainActivity
            this.txtNombreUsuario.setText("")

        }
        else Toast.makeText(applicationContext, "El nombre del trabajador es un requisito.", Toast.LENGTH_SHORT).show()
    }


    // Metodo clic para el Button: btnIngresar
    private fun btnSalir_clic(){
        var confirmar = AlertDialog.Builder(this)

        confirmar.setTitle("Pago nomina.")
        confirmar.setMessage("¿Decea cerrar la aplicación?")

        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }

        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }

        confirmar.show()
    }
}